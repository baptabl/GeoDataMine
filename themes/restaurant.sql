--METADATA={ "name:fr": "Restauration", "name:en": "Restaurant", "theme:fr": "Tourisme", "keywords:fr": [ "restaurant", "bar", "bistro", "café" ], "description:fr": "Restaurants, bars et cafés issus d'OpenStreetMap (amenity=bar|biergarten|cafe|fast_food|food_court|ice_cream|pub|restaurant)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.amenity AS type, t.tags->'name' AS name, t.tags->'operator' AS operator,
	t.tags->'brand' AS brand, t.tags->'opening_hours' AS opening_hours,
	t.wheelchair, t.tags->'cuisine' AS cuisine, t.tags->'delivery' AS delivery, t.tags->'takeaway' AS takeaway,
	t.tags->'stars' AS stars, t.tags->'capacity' AS capacity, t.tags->'drive_through' AS drive_through,
	t.tags->'wikidata' AS wikidata, t.tags->'brand:wikidata' AS brand_wikidata, t.tags->'ref:FR:SIRET' AS siret,
	t.tags->'phone' AS phone, t.tags->'website' AS website, t.tags->'facebook' AS facebook,
	t.tags->'diet:vegetarian' AS vegetarian, t.tags->'diet:vegan' AS vegan,
	t.tags->'smoking' AS smoking, t.tags->'internet_access' AS internet_access,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity IN ('bar', 'biergarten', 'cafe', 'fast_food', 'food_court', 'ice_cream', 'pub', 'restaurant') AND <GEOMCOND>
