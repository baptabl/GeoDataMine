--METADATA={ "name:fr": "Santé (hôpital, clinique, pharmacie, EHPAD)", "name:en": "Healthcare equipment", "theme:fr": "Santé", "keywords:fr": [ "hôpital", "clinique", "médecin", "pharmacie" ], "description:fr": "Établissements liés à la santé issus d'OpenStreetMap (amenity=hospital|clinic|dentist|doctors|nursing_home|pharmacy ou amenity=social_facility + social_facility=nursing_home)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id,
	COALESCE(t.tags->'social_facility', t.amenity) AS type,
	t.tags->'name' AS name,
	t.tags->'operator' AS operator, t.tags->'emergency' AS emergency, t.wheelchair,
	t.tags->'opening_hours' AS opening_hours, t.tags->'ref:FR:FINESS' AS ref_finess,
	t.tags->'type:FR:FINESS' AS type_finess, t.tags->'ref:FR:NAF' AS ref_naf, t.tags->'capacity' AS capacity,
	t.tags->'social_facility:for' AS for_public, t.tags->'ref:FR:SIRET' AS ref_siret,
	COALESCE(t.tags->'phone', t.tags->'contact:phone') AS phone,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE
	(t.amenity IN ('hospital', 'clinic', 'dentist', 'doctors', 'nursing_home', 'pharmacy')
	OR (t.amenity = 'social_facility' AND t.tags->'social_facility' = 'nursing_home'))
	AND <GEOMCOND>
