--METADATA={ "name:fr": "Déchets et recyclage (déchèteries, point d'apports ou de regroupement)", "name:en": "Waste disposal & recycling", "theme:fr": "Environnement", "keywords:fr": [ "recyclage", "point d'apport volontaire", "pav", "borne recyclage", "déchèterie", "point de regroupement" ], "description:fr": "Équipements pour le recyclage et la gestion des déchets issus d'OpenStreetMap (amenity=recycling|waste_disposal)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id,
	COALESCE(t.tags->'recycling_type', t.amenity) AS type,
	t.tags->'name' AS name,
	t.tags->'ref' AS "ref",
	t.tags->'operator' AS operator,
	t.tags->'opening_hours' AS opening_hours,
	COALESCE(t.tags->'recycling:glass', CASE WHEN t.tags->'waste' LIKE '%glass%' THEN 'yes' ELSE NULL END) AS glass,
	COALESCE(t.tags->'recycling:glass_bottles', t.tags->'recycling:glass_jars') AS glass_bottles,
	t.tags->'recycling:paper' AS paper,
	COALESCE(t.tags->'recycling:clothes', t.tags->'recycling:textiles') AS clothes,
	t.tags->'recycling:cans' AS cans,
	t.tags->'recycling:plastic_bottles' AS plastic_bottles,
	COALESCE(t.tags->'recycling:plastic', t.tags->'recycling:PET', CASE WHEN t.tags->'waste' LIKE '%plastic%' THEN 'yes' ELSE NULL END) AS plastic,
	COALESCE(t.tags->'recycling:waste', CASE WHEN t.tags->'waste' LIKE '%trash%' OR t.tags->'waste' LIKE '%mixed%' THEN 'yes' ELSE NULL END) AS waste,
	COALESCE(t.tags->'recycling:cardboard', t.tags->'recycling:cartons') AS cardboard,
	t.tags->'recycling:plastic_packaging' AS plastic_packaging,
	t.tags->'recycling:batteries' AS batteries,
	COALESCE(t.tags->'recycling:scrap_metal', t.tags->'recycling:metal') AS metal,
	COALESCE(t.tags->'recycling:newspaper', t.tags->'recycling:magazines') AS newspaper,
	t.tags->'recycling:paper_packaging' AS paper_packaging,
	COALESCE(t.tags->'recycling:green_waste', t.tags->'recycling:garden_waste') AS green_waste,
	t.tags->'recycling:shoes' AS shoes,
	COALESCE(t.tags->'recycling:organic', CASE WHEN t.tags->'waste' LIKE '%organic%' THEN 'yes' ELSE NULL END) AS organic,
	t.tags->'recycling:books' AS books,
	t.tags->'recycling:electrical_appliances' AS electrical_appliances,
	t.tags->'recycling:beverages_cartons' AS beverages_cartons,
	t.tags->'recycling:small_appliances' AS small_appliances,
	t.tags->'recycling:wood' AS wood,
	t.tags->'recycling:aluminium' AS aluminium,
	t.tags->'recycling:computers' AS computers,
	t.tags->'recycling:toys' AS toys,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity IN ('recycling', 'waste_disposal') AND <GEOMCOND>
