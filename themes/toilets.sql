--METADATA={ "name:fr": "Toilettes", "name:en": "Toilets", "theme:fr": "Infrastructures", "keywords:fr": [ "toilettes", "wc" ], "description:fr": "Toilettes publiques ou dans des établissements recevant du public issues d'OpenStreetMap (amenity=toilets ou toilets=yes, avec access!=private)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id,
	CASE WHEN t.amenity = 'toilets' THEN 'toilettes_publiques' ELSE 'toilettes_erp' END AS type,
	t.tags->'name' AS name,
	t.tags->'operator' AS operator,
	COALESCE(t.tags->'toilets:wheelchair', t.wheelchair) AS wheelchair,
	COALESCE(t.tags->'toilets:access', t.tags->'access') AS access,
	COALESCE(t.tags->'toilets:opening_hours', t.tags->'opening_hours') AS opening_hours,
	COALESCE(t.tags->'toilets:fee', t.tags->'fee') AS fee,
	COALESCE(t.tags->'toilets:supervised', t.tags->'supervised') AS supervised,
	COALESCE(t.tags->'toilets:unisex', t.tags->'unisex') AS for_everyone,
	COALESCE(t.tags->'toilets:child', t.tags->'child') AS for_child,
	COALESCE(t.tags->'toilets:female', t.tags->'female') AS for_women,
	COALESCE(t.tags->'toilets:male', t.tags->'male') AS for_men,
	t.tags->'drinking_water' AS drinking_water,
	t.tags->'toilets:disposal' AS toilets_disposal,
	t.tags->'toilets:position' AS toilets_position,
	t.tags->'toilets:paper_supplied' AS paper_supplied,
	COALESCE(t.tags->'toilets:handwashing', t.tags->'handwashing') AS handwashing,
	t.tags->'changing_table' AS changing_table,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE
	(t.amenity = 'toilets' OR t.toilets = 'yes')
	AND (t.access IS NULL OR t.access != 'private')
	AND <GEOMCOND>
