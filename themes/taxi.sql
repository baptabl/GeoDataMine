--METADATA={ "name:fr": "Stations de taxi", "name:en": "Taxi stations", "theme:fr": "Transports", "keywords:fr": [ "taxi" ], "description:fr": "Emplacement d'attente pour les taxis issus d'OpenStreetMap" }
--GEOMETRY=point,polygon

SELECT
	t.tags->'name' AS name,
	t.tags->'ref' AS ref,
	t.tags->'capacity' AS capacity,
	COALESCE(t.tags->'phone', t.tags->'contact:phone') AS phone,
	COALESCE(t.tags->'website', t.tags->'contact:website', t.tags->'brand:website') AS website,
	t.tags->'shelter' AS has_shelter,
	t.tags->'bench' AS has_bench,
	t.wheelchair,
	t.tags->'opening_hours' AS opening_hours,
	<ADM8REF> AS city_code,
	<ADM8NAME> AS city_name,
	replace(split_part(t.tags->'osm_timestamp', 'T', 1), '-', '/') AS last_update,
	<GEOM>
FROM <TABLE>
WHERE t.amenity = 'taxi' AND <GEOMCOND>
