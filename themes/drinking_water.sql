--METADATA={ "name:fr": "Point d'eau potable", "name:en": "Drinking water", "theme:fr": "Infrastructures", "keywords:fr": [ "eau", "eau potable", "robinet" ], "description:fr": "Point d'accès à l'eau potable issus d'OpenStreetMap (amenity=drinking_water|water_point ou drinking_water=yes|treated ou waterway=water_point)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id,
	t.tags->'name' AS name,
	t.tags->'operator' AS operator,
	t.tags->'bottle' AS accepts_bottle,
	t.tags->'indoor' AS indoor,
	t.tags->'cold_water' AS offers_cold_water,
	t.tags->'warmd_water' AS offers_warm_water,
	t.tags->'hot_water' AS offers_hot_water,
	t.tags->'fee' AS fee,
	t.tags->'description' AS description,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE
	(t.amenity IN('drinking_water', 'water_point') OR t.drinking_water IN ('yes', 'treated') OR t.waterway = 'water_point')
	AND <GEOMCOND>
