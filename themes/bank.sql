--METADATA={ "name:fr": "Banque et DAB", "name:en": "Bank and ATM", "theme:fr": "Économie", "keywords:fr": [ "banque", "dab", "distributeur de billets" ], "description:fr": "Banques et distributeurs de billets issus d'OpenStreetMap (amenity=bank|atm)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.amenity AS type, t.tags->'name' AS name, t.tags->'brand' AS brand,
	t.tags->'operator' AS operator, t.tags->'network' AS network, t.wheelchair,
	t.tags->'opening_hours' AS opening_hours, t.tags->'bic' AS bank_id_code,
	CASE WHEN t.amenity = 'atm' THEN 'yes' ELSE t.tags->'atm' END AS has_atm,
	t.tags->'brand:wikidata' AS brand_wikidata,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity IN ('bank', 'atm') AND <GEOMCOND>
