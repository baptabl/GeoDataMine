--METADATA={ "name:fr": "Lieu d'enseignement", "name:en": "Education", "theme:fr": "Éducation et communication", "keywords:fr": [ "école", "université", "collège", "lycée" ], "description:fr": "Lieu d'enseignement issus d'OpenStreetMap (amenity=school|kindergarten|college|university)" }
--GEOMETRY=point,polygon

SELECT
	<OSMID> as osm_id, t.tags->'name' AS name, t.tags->'amenity' AS type, t.tags->'school:FR' AS type_fr,
	t.tags->'operator:type' AS statut, t.tags->'ref:UAI' AS ref_uai, t.tags->'wikidata' AS wikidata,
	<ADM8REF> AS com_insee, <ADM8NAME> AS com_nom, <GEOM>
FROM <TABLE>
WHERE t.amenity IN ('school', 'kindergarten', 'college', 'university') AND <GEOMCOND>
